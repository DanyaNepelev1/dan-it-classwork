package lesson01;

/**
 * https://en.wikipedia.org/wiki/Two%27s_complement
 */
public class Overflow {

    public static void main(String[] args) {
        short x = 32760;
        short y = 10;
        short z = (short)(x + y);
        System.out.println(z);
    }

}
